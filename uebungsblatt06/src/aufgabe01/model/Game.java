package aufgabe01.model;

import java.util.ArrayList;
import java.util.List;

public class Game {

    public static final int MAX_FRAGEN = 5;

    private int anzahlAntwortMoeglichkeiten;
    private int aktuelleFrage = -1;
    private boolean letzteFrageRichtig = false;

    private List<Question> fragen;

    public Game(List<Question> fragen) {
        this.fragen = new ArrayList<Question>(fragen);
        this.anzahlAntwortMoeglichkeiten = 4;
    }

    public Game(List<Question> fragen, int anzahlAntwortMoeglichkeiten) {
        this.fragen = new ArrayList<Question>(fragen);
        this.anzahlAntwortMoeglichkeiten = anzahlAntwortMoeglichkeiten;
    }

    public void richtigBeantwortet() {
        this.letzteFrageRichtig = true;
    }

    public Question nextQuestion() throws IllegalStateException {
        if (this.aktuelleFrage + 1 >= MAX_FRAGEN) {
            throw new IllegalStateException("Kann keine weitere Frage mehr stellen - Spiel ist bereits vorbei!");
        }
        this.aktuelleFrage++;
        this.letzteFrageRichtig = false;

        return fragen.get(aktuelleFrage);
    }

    public boolean istGewonnen() {
        return this.aktuelleFrage == MAX_FRAGEN - 1 && this.letzteFrageRichtig;
    }

    public int getAnzahlAntwortMoeglichkeiten() {
        return this.anzahlAntwortMoeglichkeiten;
    }
}

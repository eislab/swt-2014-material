package controller;

import au.com.bytecode.opencsv.CSVReader;
import model.Answer;
import model.Game;
import model.Question;
import view.GUI;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class GameController implements ActionListener {

    private Game game;
    private Question currentQuestion;
    private GUI gui;
	private Thread delayThread;

    public GameController(Game game) {
        this.game = game;
    }

    public GameController() {
        this.initGame();
    }

	private void initGame(){
		List<Question> questions = null;
        boolean remoteError = false;
		try {
			questions = QuestionLoader.readQuestionsFromRemote(QuestionLoader.URL);
		} catch(FileNotFoundException e) {
			System.err.println("Die Datei an " + QuestionLoader.URL + " konnte nicht gefunden werden! Verwende lokale Fragenbibliothek...");
			remoteError = true;
		} catch (IOException e) {
			System.err.println("Es gab ein Problem beim Einlesen der Fragendatei. Bitte Existenz und Zugriffsrechte der Datei prüfen.");
			remoteError = true;
		}
        // We were not able to get questions.csv from remote, so we use our local question library
        if (remoteError) {
            try {
                questions = QuestionLoader.readQuestionsFromFile(QuestionLoader.DEFAULT_FILE_NAME);
            } catch (IOException e) {
                System.err.println("Die lokale Fragenbibliothek " + QuestionLoader.DEFAULT_FILE_NAME + " konnte ebenfalls nicht geladen werden. Beende Programm...");
                System.exit(-1);
            }
        }
		this.game = new Game(questions);
	}

    public void play(GUI reusableGui) {
        System.out.println("Starte...");

        if (reusableGui == null && this.gui == null) {
            this.gui = new GUI("Wer Wird Millionär");
        } else {
            this.gui = reusableGui;
	        if (this.gui != null) {
		        this.gui.initGUI();
	        }
        }
	    if (gui != null) {
		    gui.addButtonListener(this);
		    gui.setVisible(true);
		    this.proceed();
	    }
    }

    private void proceed() {
        if (!game.istGewonnen()) {

            this.currentQuestion = this.game.nextQuestion();
            gui.showQuestionAndAnswers(currentQuestion);
        } else {
            this.win();
        }
    }

    private void lose() {
        gui.showFinish("You Lose!");
    }

    private void win() {
        gui.showFinish("You Win!");
    }

    public static void main(String args[]) {
        GameController controller = new GameController();
        controller.play(null);
    }

	private void resolveAnswer(Answer answerAttempt){
		final int duration = 1000; // = 1s
		if (this.currentQuestion.isCorrectAnswer(answerAttempt)) {
			this.gui.enterCorrectAnswerState(answerAttempt);
			this.game.richtigBeantwortet();
			this.delayThread = new Thread(){
				@Override
				public void run() {
					super.run();
					try {
						sleep(duration);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					proceed();
				}
			};
		} else {
			this.gui.enterIncorrectAnswerState(this.currentQuestion.getCorrectAnswer(),answerAttempt);
			this.delayThread = new Thread(){
				@Override
				public void run() {
					super.run();
					try {
						sleep(duration);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
					lose();
				}
			};
		}
		delayThread.start();
	}

    @Override
    public void actionPerformed(ActionEvent e) {
        String action = e.getActionCommand();
	    JButton clickedButton = (JButton) e.getSource();

        int answerID = -1;
        if (action.equalsIgnoreCase(GUI.ACTION_A)) {
            answerID = 0;
        } else if (action.equalsIgnoreCase(GUI.ACTION_B)) {
            answerID = 1;
        } else if (action.equalsIgnoreCase(GUI.ACTION_C)) {
            answerID = 2;
        } else if (action.equalsIgnoreCase(GUI.ACTION_D)) {
            answerID = 3;
        }
        else if (action.equals(GUI.ACTION_RESTART)){
	        this.initGame();
	        this.play(this.gui);
	        return;
        }
	    else if(action.equals(GUI.ACTION_5050_JOKER)){
	        clickedButton.setEnabled(false);
	        List<Answer> answers = this.currentQuestion.getAnswers();
	        int disabledCount = 0;
	        for(Answer answer : answers){
		        if(!currentQuestion.isCorrectAnswer(answer)){
			        this.gui.disableButton(answer.getText());
			        disabledCount++;
		        }
		        if(disabledCount >= 2){
			        break;
		        }
	        }
	        return;
        }
	    this.gui.enterWaitState(clickedButton);

	    final int duration = 1000; // = 1s
        if (answerID <= this.currentQuestion.getAnswers().size() - 1) {
            final Answer answerAttempt = this.currentQuestion.getAnswers().get(answerID);
	        delayThread = new Thread(){
		        @Override
		        public void run() {
			        try {

				        sleep(duration);
				        resolveAnswer(answerAttempt);
			        } catch (InterruptedException e1) {
				        e1.printStackTrace();
			        }
		        }
	        };
        } else if (answerID == -1) {
	        delayThread = new Thread(){
		        @Override
		        public void run() {
			        try {
				        sleep(duration);
				        lose();
			        } catch (InterruptedException e1) {
				        e1.printStackTrace();
			        }
		        }
	        };

        }
	    if(delayThread!=null) delayThread.start();
    }
}
